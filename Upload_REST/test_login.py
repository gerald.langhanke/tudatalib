import sf.API as api

if __name__ == "__main__":
    email, password = api.ask_login_data()
    cookie = api.login(email, password)
    api.logout(cookie)
    exit()
