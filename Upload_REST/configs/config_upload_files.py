'''
For a flawless upload of many files to all the desired items the following
variables have to be set:

upload_list   which is a list of 2-element lists
e2            the 2-element list containing 1. the item id and 2. the folder
              from which all files will be upload to the item on TUdatalib

Replace <directory_path> with the WHOLE path to the folder you want to
upload files from.
ALL files in this directory will be uploaded.
'''
from pathlib import Path

#upload_list = None #python list of list of strings
upload_list =[
               ["087d3827-7b73-437d-970e-acec7b4e9520",Path("configs/test")]
              ]

'''
Please stick to this format (number of elements variable):
Using backslashes \\ in directory path in windows OS

upload_list = [
               ["<item_id>",Path(<directory_path>)],
               ["<item_id>",Path(<directory_path>)]
              ]
'''
