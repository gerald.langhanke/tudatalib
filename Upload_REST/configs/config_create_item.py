'''
If you know the collection id please write it here as python string.
Also set the metadata according to the descritpion below, as well as the
collection id.
'''
#collection_id = None #pyhton string
collection_id = "9b18efa4-a75b-45bc-b60e-e72633c81862" #pyhton string
#metadata = None #python list of dictionarys
metadata = [
            {"key":"dc.contributor.author",
             "value":"Mustermann, Max",
             "language":None
            },
            {"key":"dc.title",
             "value":"test update metadata per rest",
             "language":None
            }
            ]
                
'''
Collection id can be obtained from the "Sammlung bearbeten" page in the web
interface. If this is left as "None" you will be asked for the collection
name later.

The metadata for item creation should at least contain the dc.title keyword
as shown in the example below. Of course you can add extra metadata entrys
to the list (python list) with different keywords.
(keywords in example folder -> "metadata_keywords.txt")

Example of metadata
metadata = [{"key":"dc.title",
                     "value":"Example Item Name",
                     "language":None
                     }
                    {"key":"dc.date.issued",
                     "value":"2019",
                     "language":None
                    }
                   ]
'''
