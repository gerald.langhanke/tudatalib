'''
Set the metadata according to the descrption below
'''
#python list of strings
item_id_list = ["087d3827-7b73-437d-970e-acec7b4e9520"]

#python list of list of dictionarys
metadata_list = [
                    [{"key":"dc.description",
                     "value":"This item is inserted/updated via REST interface",
                     "language":None
                    },
                    {"key":"dc.type",
                     "value":"Other",
                     "language":None
                    },
                    {"key":"dc.date.issued",
                     "value":"2019",
                     "language":None
                    }
                   ]
                ]


'''
Item id can be obtained from the "Edit Item" page in the web
interface.
For each Item you specify you have to specify a list of metadata entrys
for this item.
Each list of metadata entrys must be in the list "metadata_list"
Each metadata entry must be a dictionary.
See examples below.
(keywords in example folder -> "metadata_keywords.txt")

Example entry for the metadata:

                    {"key":"dc.contributor.author",
                     "value":"Mustermann, Max",
                     "language":None
                    }

Example list of metadata lists:

example_metadata_list = [
                    [{"key":"dc.contributor.author",
                     "value":"Mustermann, Max",
                     "language":None
                    },
                    {"key":"dc.type",
                     "value":"Other",
                     "language":None
                    },
                    {"key":"dc.date.issued",
                     "value":"2019",
                     "language":None
                    }
                   ]
                   ]
'''
