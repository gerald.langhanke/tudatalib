These scripts are written in Python3.

They are intended to automate and standardise 
1. the creation of items in TUdatalib
1. the upload of your data files to existing items within TUdatalib
1. the addition of metadata to existing items

----------------------------------------------------------------------------------------------

The following scripts can be executed:
1. create_item.py
1. upload_files.py
1. update_metadata.py

While executing the scripts you will be forced to log in with TU-ID (or email address and password).

In general, it is necessary to fill in the config files in the "configs" folders before executing the scripts. 
1. for the script "create_item.py" fill in "config_create_item.py" (you do not neccessarily have to fill in anything here, the script will ask for the name of the new item)
1. for the script "upload_files.py" fill in "config_upload_files.py"
1. for the script "update_metadata.py" fill in "config_update_metadata.py"

You can edit these with a regular text editor like vim or notepad.

It will be checked if you filled in the information in the configs.
If you dont know how to get informations like the collection ID (which is not easily accessible if you are not a TUdatalib admin) you can go to the "helpers" folder. The scripts there can get you some of the information needed.


**Caution: Do NOT delete any files even if they are empty! This will cause the scripts to not work properly!**

For any help please contact tudata@tu-darmstadt.de!

