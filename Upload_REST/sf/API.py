'''
Author:
Marwin Acker (marwin.ack@googlemail.com)
Qin Zhao (qin.zhao@tu-darmstadt.de)

'''
#custompackages
from . import config
from . import cache
from . import utils

#packages
import requests
import getpass
from bs4 import BeautifulSoup
from xml.dom import minidom
from urllib import parse

#importing from package config
rest_url = config.rest_url

# Login
def login(email, password):
    if ('@' in email):
        cookie = loginViaEmail(email, password)
    else: 
        cookie = loginViaTUID(email, password)
    #-------check status -----------------------
    r = requests.get(rest_url+'/status', cookies = cookie)
    # read response
    tree = minidom.parseString(r.text)
    status = tree.getElementsByTagName('authenticated')[0].firstChild.data
    if ( status == 'true' ):
        fullname = tree.getElementsByTagName('fullname')[0].firstChild.data
        print('Hello ' + fullname + ', welcome to TUdatalib REST!')
    else:
        print('login failed');
    return cookie

# Login via email address
def loginViaEmail(email, password):
    logindata = {"email":email,"password":password}
    cookie = requests.post(rest_url+'/login', data = logindata).cookies
    return cookie

#Login with given email adress and passwort, returns session cookie
def loginViaTUID(tuid, password):
    # build a session
    session = requests.session()

    #------------ get shibboleth-login site -----------------------
    r1 = session.get(rest_url+'/shibboleth-login', allow_redirects=False)

    if r1.status_code == 302: # should redirect to SSO login        
        jar = session.cookies
        
        # ----------- request get redirect url-----------------------
        redirect_url2 = r1.headers['Location']        
        res2 = session.get(redirect_url2, cookies=jar)
        # ---- set entity id instead of choose entity in JavaScript-----
        redirect_url3 = parse.parse_qs(parse.urlparse(redirect_url2).query)['return'][0]
        res3 = session.get(redirect_url3 + "&entityID=https%3A%2F%2Fidp.hrz.tu-darmstadt.de%2Fidp%2Fshibboleth", cookies=jar)

        #-------------post login data-----------------------
        soup = BeautifulSoup(res3.text, "lxml")
        value = soup.find('input', {'name':'csrf_token'}).get('value')
        logindata = {"j_username":tuid,"j_password":password,"csrf_token":value, "_eventId_proceed":"submit"}
        res4 = session.post(config.sso_url + '/idp/profile/SAML2/Redirect/SSO?execution=e1s1', cookies = jar, data = logindata)

        #----if there is e1s2 ?----#

        #----------post saml response to tudatalib-----------------------
        soup = BeautifulSoup(res4.text, "lxml")
        action = soup.find('form',{'method':'post'}).get('action')
        samlresponse = soup.find('input', {'name':'SAMLResponse'}).get('value')
        relayState = soup.find('input', {'name':'RelayState'}).get('value')
        logindata = {'RelayState': relayState, 'SAMLResponse':samlresponse}
        r4 = requests.post(action, cookies = jar, data = logindata)
        # get cookies
        cookie = r4.cookies
        
        return cookie

#logs out using the cookie you got from login
def logout(cookie):
    requests.get(rest_url+'/logout', cookies = cookie)
    requests.get(config.sso_url+'/logout', cookies = cookie)

def availability():
    r = requests.get(rest_url+'/test')
    if r.text.lower() != 'rest api is running.':
        raise Exception('REST API not running. The response code was: {}.'.format(r.status_code))
        raise Exception('Please contact an administrator') #Which is not Marwin Acker
        exit()
    return r

#aks for login data.
#This saves the email to the config and later asks you if you want to use this
#or give a new one
def ask_login_data():
    email = cache.email
    if email == None:
        email = input("Enter TU-ID or EMail address: ")
    else:
        use_cache = input("Do you want to use '{}' as username? (y/n) ".format(cache.email))
        if use_cache == "y":
            email = cache.email
        elif use_cache == "n":
            email = input("Enter TU-ID or EMail address: ")
        else:
            print("invalid input")
            ask_login_data()
    password = getpass.getpass("Enter password: ")
    utils.write_to_cache(email,"email")
    return email, password

#This uploads a file, given its path, to an bitstream, given its ID
#this also needs the session cookie
def upload_to_bitstream(file_path, bitstream_id, cookie):
    r = requests.put(rest_url+'/bitstreams/'+bitstream_id+'/data',
                     cookies = cookie, data = open(file_path, 'rb'))
    return r

#creates bitstream to item, given its ID. Returns the bitstream id
def create_bitstream(item_id, filename, cookie):
    r = requests.post(rest_url+'/items/'+item_id+'/bitstreams?name='+filename,
                      cookies = cookie)
    _,_,leftover = r.text.partition('<bitstream><link>/rest/bitstreams/')
    bitstream_id,_,_ = leftover.partition('</link>')
    return bitstream_id

#uploads file to item
def upload(item_id, file_path, cookie):
    file_name = file_path.split("/")[-1] #filename is last in path to file
    bitstream_id = create_bitstream(item_id,file_name, cookie)
    response = upload_to_bitstream(file_path, bitstream_id, cookie)
    return response
    #create_bitstream(item_id, file)

#creates item given collection id and metadata of item and cookie
def create_item(collection_id, metadata, cookie):
    item = {"metadata":metadata}
    r = requests.post(rest_url+'/collections/'+collection_id+'/items',
                      cookies = cookie, json = item)
    return r

#makes a request to TUdatalibs rest api to get all public collections
def get_collections(cookie):
    r = requests.get(rest_url+"/collections",cookies = cookie)
    #print(r.text)
    collections_dict = utils.get_collections_dict(r)
    return collections_dict

#checks if a collection exists given its ID, returns True if so
def collection_exists(collection_id, cookie):
    r = requests.get('{}/collections/{}'.format(rest_url,collection_id),
                     cookies = cookie)
    if str(r.content) == 'b' '' or 'b''' == str(r.content):
        return False
    else: return True

#uploads given metadata to given item (id), returns response object
def upload_metadata(item_id, metadata, cookie):
    r = requests.post('{}/items/{}/metadata'.format(rest_url,item_id),
                        cookies = cookie, json = metadata)
    return r
